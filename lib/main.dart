import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

import 'package:bluetooth_print/bluetooth_print.dart';
import 'package:bluetooth_print/bluetooth_print_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'dart:ui' as ui;

import 'package:path_provider/path_provider.dart';

void main() => runApp(MaterialApppppp());

class MaterialApppppp extends StatelessWidget {
  const MaterialApppppp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'lol',
      home: MyApp(),
    );
  }
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  BluetoothPrint bluetoothPrint = BluetoothPrint.instance;

  bool _connected = false;
  BluetoothDevice _device;
  String tips = 'no device connect';
  GlobalKey _globalKey = GlobalKey();
  Widget check;
  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((_) => initBluetooth());
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initBluetooth() async {
    bluetoothPrint.startScan(timeout: Duration(seconds: 4));

    bool isConnected = await bluetoothPrint.isConnected;

    bluetoothPrint.state.listen((state) {
      print('cur device status: $state');

      switch (state) {
        case BluetoothPrint.CONNECTED:
          setState(() {
            _connected = true;
            tips = 'connect success';
          });
          break;
        case BluetoothPrint.DISCONNECTED:
          setState(() {
            _connected = false;
            tips = 'disconnect success';
          });
          break;
        default:
          break;
      }
    });

    if (!mounted) return;

    if (isConnected) {
      setState(() {
        _connected = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('BluetoothPrint example app'),
      ),
      body: RefreshIndicator(
        onRefresh: () =>
            bluetoothPrint.startScan(timeout: Duration(seconds: 4)),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
                    child: Text(tips),
                  ),
                ],
              ),
              Divider(),
              StreamBuilder<List<BluetoothDevice>>(
                stream: bluetoothPrint.scanResults,
                initialData: [],
                builder: (c, snapshot) => Column(
                  children: snapshot.data
                      .map((d) => ListTile(
                            title: Text(d.name ?? ''),
                            subtitle: Text(d.address),
                            onTap: () async {
                              setState(() {
                                _device = d;
                              });
                            },
                            trailing:
                                _device != null && _device.address == d.address
                                    ? Icon(
                                        Icons.check,
                                        color: Colors.green,
                                      )
                                    : null,
                          ))
                      .toList(),
                ),
              ),
              Divider(),
              Container(
                padding: EdgeInsets.fromLTRB(20, 5, 20, 10),
                child: Column(
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        OutlineButton(
                          child: Text('connect'),
                          onPressed: _connected
                              ? null
                              : () async {
                                  if (_device != null &&
                                      _device.address != null) {
                                    await bluetoothPrint.connect(_device);
                                  } else {
                                    setState(() {
                                      tips = 'please select device';
                                    });
                                    print('please select device');
                                  }
                                },
                        ),
                        SizedBox(width: 10.0),
                        OutlineButton(
                          child: Text('disconnect'),
                          onPressed: _connected
                              ? () async {
                                  await bluetoothPrint.disconnect();
                                }
                              : null,
                        ),
                      ],
                    ),
                    OutlineButton(
                      child: Text('print receipt(esc)'),
                      onPressed: _connected
                          ? () async {
                              Map<String, dynamic> config = Map();
                              List<LineText> list = List();
                              list.add(LineText(
                                  type: LineText.TYPE_TEXT,
                                  content: 'A Title',
                                  weight: 1,
                                  align: LineText.ALIGN_CENTER,
                                  linefeed: 1));
                              list.add(LineText(
                                type: LineText.TYPE_TEXT,
                                content:
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890'
                                    '1234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890',
                                x: 25,
                                y: 0,
                                // align: LineText.ALIGN_LEFT,
                                linefeed: 1,
                              ));
                              // list.add(LineText(
                              //   type: LineText.TYPE_TEXT,
                              //   content: 'this is conent right',
                              //   align: LineText.ALIGN_RIGHT,
                              //   linefeed: 1,
                              // ));
                              // list.add(LineText(linefeed: 1));
                              // list.add(LineText(
                              //     type: LineText.TYPE_BARCODE,
                              //     content: 'A12312112',
                              //     size: 10,
                              //     align: LineText.ALIGN_CENTER,
                              //     linefeed: 1));
                              // list.add(LineText(linefeed: 1));
                              // list.add(LineText(
                              //     type: LineText.TYPE_QRCODE,
                              //     content: 'qrcode i',
                              //     size: 10,
                              //     align: LineText.ALIGN_CENTER,
                              //     linefeed: 1));
                              // list.add(LineText(linefeed: 1));
                              await bluetoothPrint.printReceipt(config, list);
                            }
                          : null,
                    ),
                    OutlineButton(
                      child: Text('print label(tsc)'),
                      onPressed: _connected
                          ? () async {
                              Map<String, dynamic> config = Map();
                              config['width'] = 40; // 标签宽度，单位mm
                              config['height'] = 70; // 标签高度，单位mm
                              config['gap'] = 2; // 标签间隔，单位mm

                              // x、y坐标位置，单位dpi，1mm=8dpi
                              List<LineText> list = List();
                              list.add(LineText(
                                  type: LineText.TYPE_TEXT,
                                  x: 10,
                                  y: 10,
                                  content: 'A Title'));

                              List<LineText> list1 = List();
                              ByteData data = await rootBundle
                                  .load("assets/images/guide3.png");
                              List<int> imageBytes = data.buffer.asUint8List(
                                  data.offsetInBytes, data.lengthInBytes);
                              String base64Image = base64Encode(imageBytes);
                              list1.add(LineText(
                                type: LineText.TYPE_IMAGE,
                                x: 10,
                                y: 10,
                                content: base64Image,
                              ));

                              // await bluetoothPrint.printLabel(config, list);
                              await bluetoothPrint.printLabel(config, list1);
                            }
                          : null,
                    ),
                    OutlineButton(
                      child: Text('print selftest'),
                      onPressed: _connected
                          ? () async {
                              Map<String, dynamic> config = Map();
                              config['width'] = 40; // 标签宽度，单位mm
                              config['height'] = 70; // 标签高度，单位mm
                              config['gap'] = 2;
                              setState(() {
                                check = createImage();
                              });
                              Future.delayed(Duration(seconds: 2), () async {
                                RenderRepaintBoundary boundary = _globalKey
                                    .currentContext
                                    .findRenderObject();
                                ui.Image image =
                                    await boundary.toImage(pixelRatio: 1.0);

                                // final directory =
                                // await getApplicationDocumentsDirectory();
                                // File f = File('${directory.path}/check.png');
                                // File f = File(
                                //     '/storage/emulated/0/Download/check.png');
                                // // image.
                                // print(f.uri);
                                print('${image.width}, ${image.height}');
                                ByteData byteData = await image.toByteData(
                                    format: ui.ImageByteFormat.png);
                                List<int> imageBytes =
                                    byteData.buffer.asUint8List(
                                        byteData.offsetInBytes,
                                        // .asInt8List(byteData.offsetInBytes,
                                        byteData.lengthInBytes);
                                // f.writeAsBytesSync(imageBytes);
                                // List<int> bytes = await f.readAsBytes();
                                // Uint8List pngBytes =
                                //     byteData.buffer.asInt8List();
                                // String bs64 = base64Encode(bytes);
                                String bs64 = base64Encode(imageBytes);
                                String imagBase = bs64;
                                // String imagBase = await getBase64Image();
                                print(imagBase);
                                List<LineText> list1 = List();
                                list1.add(LineText(
                                  type: LineText.TYPE_IMAGE,
                                  x: 10,
                                  y: 10,
                                  content: imagBase,
                                ));
                                if (imagBase != null && imagBase.isNotEmpty) {
                                  await showDialog<void>(
                                    context: context,
                                    builder: (_) => Dialog(
                                      child: Image.memory(imageBytes),
                                    ),
                                  );
                                  await bluetoothPrint.printLabel(
                                      config, list1);
                                }
                              });
                            }
                          : null,
                    ),
                    check != null ? check : Container(),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
      floatingActionButton: StreamBuilder<bool>(
        stream: bluetoothPrint.isScanning,
        initialData: false,
        builder: (c, snapshot) {
          if (snapshot.data) {
            return FloatingActionButton(
              child: Icon(Icons.stop),
              onPressed: () => bluetoothPrint.stopScan(),
              backgroundColor: Colors.red,
            );
          } else {
            return FloatingActionButton(
                child: Icon(Icons.search),
                onPressed: () =>
                    bluetoothPrint.startScan(timeout: Duration(seconds: 4)));
          }
        },
      ),
    );
  }

  Future<String> getBase64Image() async {
    RenderRepaintBoundary boundary =
        _globalKey.currentContext.findRenderObject();
    ui.Image image = await boundary.toImage(pixelRatio: 3.0);
    ByteData byteData = await image.toByteData(format: ui.ImageByteFormat.png);
    Uint8List pngBytes = byteData.buffer.asUint8List();
    String bs64 = base64Encode(pngBytes);
    return bs64;
  }

  Widget createImage() {
    return Container(
      width: 350,
      child: RepaintBoundary(
        key: _globalKey,
        child: Container(
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              // Row(
              //   children: <Widget>[
              //     Expanded(
              //       flex: 1,
              //       child: Image.asset(
              //         'assets/images/icon.png',
              //       ),
              //     ),
              //     Spacer(flex: 1),
              //   ],
              // ),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Flexible(
                    flex: 6,
                    child: Text(
                      'Margarita pizza',
                      style: TextStyle(
                        fontSize: 20.0,
                      ),
                    ),
                  ),
                  Spacer(flex: 1),
                  Flexible(
                    flex: 3,
                    child: Text(
                      '50.00',
                      style: TextStyle(
                        fontSize: 22.0,
                      ),
                    ),
                  ),
                ],
              ),
              Text(
                '5 x 10.00',
                style: TextStyle(
                  fontSize: 22.0,
                ),
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Flexible(
                    flex: 6,
                    child: Text(
                      'This is very very very very very very very very very very very very very very very very very very very very very long Neapolitan pizza',
                      style: TextStyle(
                        fontSize: 20.0,
                      ),
                    ),
                  ),
                  Spacer(flex: 1),
                  Flexible(
                    flex: 3,
                    child: Text(
                      '25.00',
                      style: TextStyle(
                        fontSize: 22.0,
                      ),
                    ),
                  ),
                ],
              ),
              Text(
                '2 x 12.50',
                style: TextStyle(
                  fontSize: 22.0,
                ),
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Flexible(
                    flex: 6,
                    child: Text(
                      'Cola',
                      style: TextStyle(
                        fontSize: 20.0,
                      ),
                    ),
                  ),
                  Spacer(flex: 1),
                  Flexible(
                    flex: 3,
                    child: Text(
                      '5.00',
                      style: TextStyle(
                        fontSize: 22.0,
                      ),
                    ),
                  ),
                ],
              ),
              Text(
                '1 x 5.00',
                style: TextStyle(
                  fontSize: 22.0,
                ),
              ),
              SizedBox(height: 25),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Flexible(
                    flex: 6,
                    child: Text(
                      'Subtotal',
                      style: TextStyle(
                        fontSize: 20.0,
                      ),
                    ),
                  ),
                  Spacer(flex: 1),
                  Flexible(
                    flex: 3,
                    child: Text(
                      '80.00',
                      style: TextStyle(
                        fontSize: 22.0,
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Flexible(
                    flex: 6,
                    child: Text(
                      'Discount',
                      style: TextStyle(
                        fontSize: 20.0,
                      ),
                    ),
                  ),
                  Spacer(flex: 1),
                  Flexible(
                    flex: 3,
                    child: Text(
                      '0.00',
                      style: TextStyle(
                        fontSize: 22.0,
                      ),
                    ),
                  ),
                ],
              ),
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Flexible(
                    flex: 6,
                    child: Text(
                      'Total',
                      style: TextStyle(
                        fontSize: 30.0,
                      ),
                    ),
                  ),
                  Spacer(flex: 1),
                  Flexible(
                    flex: 3,
                    child: Text(
                      '0.00',
                      style: TextStyle(
                        fontSize: 30.0,
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
